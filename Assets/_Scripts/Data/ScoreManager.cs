﻿using UnityEngine;

/// <summary>
/// Class responsible for storing the score in the local database
/// </summary>
public class ScoreManager : MonoBehaviour
{
	/// <summary>
	/// Called when database is setup and usable
	/// </summary>
	public static System.Action OnScoreAvailable;

	private void Start()
	{
		SQLite.singleton.CreateDatabase("Score.sqlite");
        SQLite.singleton.SetCurrentDatabase("Score.sqlite", 
        ()=>
        {
            System.Collections.Generic.Dictionary<string, string> fields = new System.Collections.Generic.Dictionary<string, string>();
            fields.Add("id", "INTEGER PRIMARY KEY AUTOINCREMENT");
            fields.Add("score", "FLOAT NOT NULL");
            fields.Add("levelID", "UNSIGNED INTEGER NOT NULL");
            SQLite.singleton.CreateTable("score", fields);
			OnScoreAvailable?.Invoke();
        },
        (SQLite.FailReason reason)=>
        	UnityEngine.Debug.Log(reason.ToString()));


		Timer.OnTimerStart += ()=>
			Timer.OnTimerEnd += this.OnScoreCalculated;
	}

	private void OnScoreCalculated(float time)
	{
		Timer.OnTimerEnd -= this.OnScoreCalculated;
		if(!DATA.LEVEL_COMPLETED)
			return;
		
		ScorescreenManager.singleton.ShowScore(time);
		
		string timeString = time.ToString(new System.Globalization.CultureInfo("en-US"));
		System.Collections.Generic.Dictionary<int, object[]> data = SQLite.singleton.Select($"SELECT * FROM `score` WHERE `levelID` = {DATA.CURRENT_LEVEL}");
		if(data.Count < 1)
			SQLite.singleton.RunCommand($"INSERT INTO `score`(`id`, `score`, `levelID`) VALUES(NULL, {timeString}, {DATA.CURRENT_LEVEL});");
		else
		{
			float currentScore = (float)data[0][1];
			if(time < currentScore)
				SQLite.singleton.RunCommand($"UPDATE `score` SET `score` = {timeString} WHERE `levelID` = {DATA.CURRENT_LEVEL};");
		}
	}
};