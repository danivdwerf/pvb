﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]protected Canvas screen = null;
    public Canvas Screen{get=> this.screen;}

    [SerializeField]private bool ignoreController = false;
    public bool IgnoreController{get=> this.ignoreController;}

    private RectTransform[] layoutGroups;
    public bool IsEnabled{get=> this.screen == null ? false : this.screen.gameObject.activeInHierarchy;}

    protected virtual void Awake()
    {
        if(this.screen is null)
        {
            Debug.Log($"{this} has no screen assigned");
            return;
        }

        this.screen.gameObject.SetActive(false);

        UnityEngine.UI.LayoutGroup[] groups = this.screen.GetComponentsInChildren<UnityEngine.UI.LayoutGroup>(true);
        int len = groups.Length;
        this.layoutGroups = new RectTransform[len];
        for(int i = 0; i < len; i++) 
            this.layoutGroups[i] = groups[i].GetComponent<RectTransform>();
    }

    public virtual void Setup(Camera camera, Vector2 targetResolution)
    {
        if(this.screen is null)
            return;
        
        this.screen.worldCamera = camera;
        this.screen.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = targetResolution;
    }

    /// <summary>
    /// Show or hides the screen bases on the given values.
    /// </summary>
    /// <param name="value">True if the screen must become visible</param>
    /// <param name="force">True if OnScreenEnabled must be called even if the screen is already visible</param>
    public virtual void Show(bool value, bool force = false)
    {
        if(this.screen is null)
        {
            Debug.Log($"{this} has no screen assigned");
            return;
        }

        if(this.screen.gameObject.activeInHierarchy.Equals(value) && !force) 
            return;
        
        this.Activate(value);

        if(value) 
            this.OnScreenEnabled();
        else 
            this.OnScreenDisabled();
    }

    protected virtual void Activate(bool value)=> this.screen.gameObject.SetActive(value);
    
    protected virtual void OnScreenEnabled(){}
	protected virtual void OnScreenDisabled(){}

    /// <summary>
    /// Force layout elements to rebuild
    /// </summary>
    protected void UpdateLayout()
    {
        if(this.layoutGroups is null) 
            return;

        int len = this.layoutGroups.Length;
        for(int i = 0; i < len; i++) 
            UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(this.layoutGroups[i]);
    }

    /// <summary>
    /// Get the type to pass to the UIController
    /// </summary>
    /// <returns>Manager's type</returns>
    public string ToType()=> 
        this.GetType().ToString().ToUpper().Replace("MANAGER", string.Empty);
};