﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UIManager to instruct the player to scan de marker
/// </summary>
public class PopupscreenManager : UIManager
{
	public static PopupscreenManager singleton;
	/// <summary>
	/// Called when player presses the close button.
	/// </summary>
	public static System.Action OnClose;

	[SerializeField]private UIButton closeButton = default;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}   
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying)
			return;

		this.closeButton.OnClick += this.CloseWindow;
	}

	private void CloseWindow()
	{
		this.Show(false);
		OnClose?.Invoke();
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying)
			return;

		this.closeButton.OnClick -= this.CloseWindow;
	}
};