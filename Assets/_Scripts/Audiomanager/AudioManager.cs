﻿using UnityEngine;

/// <summary>
/// Central manager to have control over all in-game audio
/// </summary>
public class AudioManager : MonoBehaviour 
{
	public static AudioManager singleton;

	[SerializeField]private System.Collections.Generic.List<AudioClip> audioClips = null;
	/// <summary>
	/// Current list of Audioclips
	/// </summary>
	public System.Collections.Generic.List<AudioClip> AudioClips{get=> this.audioClips;}

	private System.Collections.Generic.Dictionary<string, AudioClip> clips = null;

	[SerializeField]private System.Collections.Generic.List<string> audioTags = null;
	/// <summary>
	/// List of available audio tags
	/// </summary>
	public System.Collections.Generic.List<string> AudioTags{get=> this.audioTags;}
	
	private void Awake()
	{
		if(singleton != null && singleton != this) 
			Destroy(this);
		singleton = this;

		this.SetupClips();
	}

	private void SetupClips()
	{
		this.clips = new System.Collections.Generic.Dictionary<string, AudioClip>();

		int len = this.audioClips.Count;
		for(int i = 0; i < len; i++) 
			this.AddClip(this.audioClips[i]);
		this.audioClips = null;
	}

	/// <summary>
	/// Add audioclip to the dictionary of clips
	/// </summary>
	/// <seealso cref="AudioClip"/>
	/// <param name="clipToAdd">The audioclip to add</param>
	public void AddClip(AudioClip clipToAdd)
	{
		string name = clipToAdd.Name;
		if(this.clips.ContainsKey(name))
		{
			int index = 1;
			while(this.clips.ContainsKey($"{name}_{index}")) 
				index++;

			name = $"{name}_{index}";
		}

		GameObject source = new GameObject($"[AUDIOCLIP]: {name}");
		source.transform.SetParent(this.transform, false);
		clipToAdd.SetSource(source.AddComponent<AudioSource>());
		this.clips.Add(name, clipToAdd);
	}

	/// <summary>
	/// Play a clip by the given name
	/// </summary>
	/// <param name="name">The name of the clip</param>
	public void PlayClip(string name)
	{
		if(!this.clips.ContainsKey(name)) 
		{
			Debug.LogWarning($"The audioclip \"{name}\" does not exist.");
			return;
		}

		this.clips[name].Play();
	}

	/// <summary>
	/// Stop a clip by the given name
	/// </summary>
	/// <param name="name">The name of the clip</param>
	public void StopClip(string name)
	{
		if(!this.clips.ContainsKey(name)) 
		{
			Debug.LogWarning($"The audioclip \"{name}\" does not exist.");
			return;
		}
		
		this.clips[name].Stop();
	}
	
	/// <summary>
	/// Check if the clip with the given name is currently playing
	/// </summary>
	/// <param name="name">The name of the clip</param>
	/// <returns>True if the clip is currently playing</returns>
	public bool IsClipPlaying(string name)
	{
		if(!this.clips.ContainsKey(name)) 
		{	
			Debug.LogWarning($"The audioclip \"{name}\" does not exist.");
			return false;
		}
		
		return this.clips[name].IsPlaying;
	}

	/// <summary>
	/// Sets all the clip's volumes to a scaled value of it's volume. Set tag to only update the given tag.
	/// </summary>
	/// <param name="multiplier">The value to multiply the current volume with</param>
	/// <param name="tag">DATA.AUDIO_TAGS</param>
	public void SetVolumeMultiplier(float multiplier, string tag = "")
	{
		int len = this.clips.Count;
		string[] keys = new string[len];
		this.clips.Keys.CopyTo(keys, 0);
		for(int i = 0; i < len; i++)
		{
			string key = keys[i];
			if(!string.IsNullOrEmpty(tag) && this.clips[key].Tag != this.audioTags.IndexOf(tag))
				continue;

			this.clips[key].SetVolumeMultiplier(multiplier);
		}
	}

	/// <summary>
	/// Returns the length of an AudioClip
	/// </summary>
	/// <param name="name">The name of the clip</param>
	/// <returns>The length of the clip</returns>
	public float GetLength(string name)
	{
		if(!this.clips.ContainsKey(name)) 
		{
			Debug.LogWarning($"The audioclip \"{name}\" does not exist.");
			return -1.0f;
		}

		return this.clips[name].Length;
	}
};