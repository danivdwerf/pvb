﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UIManager to give the player instructions
/// </summary>
public class HelpscreenManager : UIManager
{
	public static HelpscreenManager singleton;

	[Header("Navigation")]
	[SerializeField]private UIButton previousButton = default;
	[SerializeField]private UIButton nextButton = default;

	[Header("Screens")]
	[SerializeField]private RectTransform imageHolder = default;
	[SerializeField]private Sprite[] screens = default;
	[SerializeField]private UIButton closePrefab = default;

	[Header("Transition")]
	[SerializeField]private float transitionDuration = default;
	[SerializeField]private AnimationCurve transitionCurve = default;

	private int currentIndex;
	private bool isSetup;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}

	private void Start()
	{
		this.imageHolder.ClearChildren();
		int len = this.screens.Length;
		for(int i = 0; i < len; i++)
		{
			GameObject screen = new GameObject("[IMAGE] Screen");
			RectTransform transform = screen.AddComponent<RectTransform>();
			transform.SetParent(this.imageHolder, false);

			transform.sizeDelta = new Vector2(1598.0f, 852.0f);
			
			Image image = screen.AddComponent<Image>();
			image.sprite = this.screens[i];

			UIButton close = GameObject.Instantiate<UIButton>(this.closePrefab);
			close.transform.SetParent(screen.transform, false);
			close.OnClick = ()=>
				UIController.singleton.GoToScreen<HUDscreenManager>();
		}
	}
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying)
			return;

		if(!this.isSetup)
		{
			HorizontalLayoutGroup layout =  this.imageHolder.GetComponent<HorizontalLayoutGroup>();
			int offset = Mathf.RoundToInt((((RectTransform)this.screen.transform).sizeDelta.x - 1598.0f) / 2.0f);
			layout.padding = new RectOffset(offset, offset, 0, 0);
			layout.spacing = offset * 2;
			this.isSetup = true;
		}

		this.GoToSlide(0);
		this.previousButton.OnClick = ()=> 
			this.GoToSlide(this.currentIndex - 1);
		this.nextButton.OnClick = ()=> 
			this.GoToSlide(this.currentIndex + 1);
	}


	private void GoToSlide(int index)
	{
		if(index < 0 || index >= this.screens.Length)
			return;

		this.currentIndex = index;
		this.previousButton.gameObject.SetActive(index > 0);
		this.nextButton.gameObject.SetActive(index < this.screens.Length - 1);

		System.Collections.IEnumerator Move()
		{
			Vector2 from = this.imageHolder.anchoredPosition;
			Vector2 to = new Vector2(index * -((RectTransform)this.screen.transform).sizeDelta.x, from.y);
			Vector2 pos = to;
			float delta = to.x - from.x;
			float timer = 0.0f;

			this.imageHolder.anchoredPosition = pos;
			while(timer <= this.transitionDuration)
			{
				float step = delta * this.transitionCurve.Evaluate(timer / this.transitionDuration);
				pos.x = from.x + step;
				this.imageHolder.anchoredPosition = pos;

				timer+=Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
		};
		StartCoroutine(Move());
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying)
			return;
	}
};