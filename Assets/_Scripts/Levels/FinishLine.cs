﻿using UnityEngine;

/// <summary>
/// Class to detect if player has reached the finish.
/// </summary>
public class FinishLine : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(!other.tag.Equals(TAGS.PLAYER_TAG))
            return;

        DATA.LEVEL_COMPLETED = true;
        Timer.singleton.StopTimer();
    }
};