using UnityEngine;

/// <summary>
/// Class to combine plane detection and target detection.
/// </summary>
public class PlaneDetectionImage : MonoBehaviour, Vuforia.ITrackableEventHandler
{
    [SerializeField]private RepositionLevel repositionLevel = default;
    [SerializeField]private Vuforia.PlaneFinderBehaviour planeFinderBehaviour = default;

    private bool hasFoundTarget;
    /// <summary>
    /// Has the class found the image target?
    /// </summary>
    public bool HasFoundTarget{get=> this.hasFoundTarget; set=> this.hasFoundTarget = value;}

    private bool shouldLookForTarget;
    /// <summary>
    /// Should the class look for an image target?
    /// </summary>
    public bool ShouldLookForTarget{get=> this.shouldLookForTarget; set=> this.shouldLookForTarget = value;}
    
    private Vuforia.HitTestResult currentHitTest;
    private Vuforia.TrackableBehaviour trackableBehaviour;

    private void Awake()
    {
        this.shouldLookForTarget = true;
        this.trackableBehaviour = this.GetComponent<Vuforia.TrackableBehaviour>();

        if(this.trackableBehaviour)
            this.trackableBehaviour.RegisterTrackableEventHandler(this);
    }

    private void OnEnable()
    {
        HUDscreenManager.OnRecenter += this.RepositionLevel;
        this.planeFinderBehaviour.OnAutomaticHitTest.AddListener(this.OnPlaneFound);
    }

    private void OnPlaneFound(Vuforia.HitTestResult hitTest)
    {
        this.currentHitTest = hitTest;
        if(this.hasFoundTarget && this.shouldLookForTarget)
            this.InstantiateLevel();
    }

    private void InstantiateLevel()
    {
        this.shouldLookForTarget = false;
        this.repositionLevel.LevelActive = true;
        this.RepositionLevel();
    }

    private void RepositionLevel()
    {
        if(this.currentHitTest == null)
            return;

        this.repositionLevel.ReposLevel(this.currentHitTest);
    }

    /// <summary>
    /// Implement Vuforia.ITrackableEventHandler
    /// </summary>
    public void OnTrackableStateChanged(Vuforia.TrackableBehaviour.Status previousStatus, Vuforia.TrackableBehaviour.Status newStatus)=>
        this.hasFoundTarget = newStatus.Equals(Vuforia.TrackableBehaviour.Status.DETECTED) || newStatus.Equals(Vuforia.TrackableBehaviour.Status.TRACKED);

    private void OnDisable()
    {
        HUDscreenManager.OnRecenter -= this.RepositionLevel;
        this.planeFinderBehaviour.OnAutomaticHitTest.RemoveListener(this.OnPlaneFound);
    }

    private void OnDestroy()
    {
        if(this.trackableBehaviour)
            this.trackableBehaviour.UnregisterTrackableEventHandler(this);
    }
};