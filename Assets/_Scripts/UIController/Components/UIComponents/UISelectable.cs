using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Base class to build UI elements
/// </summary>
public abstract class UISelectable : MonoBehaviour, UnityEngine.EventSystems.IPointerEnterHandler, UnityEngine.EventSystems.IPointerExitHandler
{
    public enum TransitionType{NONE = 1, COLOUR = 2, SPRITE = 4};

    #region Callbacks
    /// <summary>
    /// Callback when the mouse enters the selectable
    /// </summary>
    public System.Action OnMouseEnter;

    /// <summary>
    /// Callback when the mouse exists the selectable
    /// </summary>
    public System.Action OnMouseExit;
    #endregion

    #region Transitions
    [SerializeField]protected TransitionType transitionType = TransitionType.SPRITE;

    #region Colours
    [SerializeField]protected Color normalColour = Color.white;
    [SerializeField]private Color hoverColour = Color.white;
    [SerializeField]private Color disabledColour = Color.white;
    #endregion

    #region Sprites
    protected Graphic targetGraphic = null;
    /// <summary>
    /// The graphic to check the events on
    /// </summary>
    public Graphic Graphic
    {
        get
        {
            if(this.targetGraphic is null) 
                this.UpdateGraphic();
                
            return this.targetGraphic;
        }
    }
    
    protected System.Reflection.PropertyInfo spriteInfo;

    protected Sprite normalSprite = null;
    [SerializeField]private Sprite hoverSprite = null;
    [SerializeField]private Sprite disabledSprite = null;
    #endregion
    #endregion

    #region Cursor
    [SerializeField]private Texture2D cursorTexture = null;
    private Vector2 cursorCenter = Vector2.zero;
    #endregion

    protected virtual void Awake()
    {
        if(this.targetGraphic == null) 
            this.UpdateGraphic();

        if(this.cursorTexture != null)
            this.cursorCenter = new Vector2(this.cursorTexture.width/2, this.cursorTexture.height/2);
    }

    /// <summary>
    /// Get the button's graphic
    /// </summary>
    private void UpdateGraphic()
    {
        this.targetGraphic = this.gameObject.GetComponent<Graphic>();
        this.targetGraphic.raycastTarget = true;

        System.Type graphicType = this.targetGraphic.GetType();
        System.Reflection.PropertyInfo info = graphicType.GetProperty("sprite");

        this.spriteInfo = info;
        this.normalSprite = (Sprite)this.spriteInfo.GetValue(this.targetGraphic);
    }

    /// <summary>
    /// Set the normal sprite to the given sprite
    /// </summary>
    /// <param name="sprite">New sprite</param>
    public void SetSprite(Sprite sprite)
    {
        if(this.targetGraphic is null) 
        {
            this.UpdateGraphic();
            if(this.targetGraphic is null) 
                return;
        }
        
        this.normalSprite = sprite;
        this.spriteInfo.SetValue(this.targetGraphic, sprite);
    }

    protected virtual void OnEnable()
    {
        if((this.transitionType & TransitionType.SPRITE) != 0)
            this.spriteInfo.SetValue(this.targetGraphic, this.normalSprite);

        if((this.transitionType & TransitionType.COLOUR) != 0)
            this.targetGraphic.color = this.normalColour;
    }
    
    public virtual void Submit(){}

    public virtual void OnPointerEnter(UnityEngine.EventSystems.PointerEventData e)
    {
        if(e.button != UnityEngine.EventSystems.PointerEventData.InputButton.Left || !this.enabled) 
            return;
        
        if((this.transitionType & TransitionType.SPRITE) != 0 && !(this.hoverSprite is null))
            this.spriteInfo.SetValue(this.targetGraphic, this.hoverSprite);

        if((this.transitionType & TransitionType.COLOUR) != 0) 
            this.targetGraphic.color = this.hoverColour;

        if(this.cursorTexture != null)
            Cursor.SetCursor(this.cursorTexture, this.cursorCenter, CursorMode.ForceSoftware);

        this.OnMouseEnter?.Invoke();
    }

    public virtual void OnPointerExit(UnityEngine.EventSystems.PointerEventData e)
    {
        if(e.button != UnityEngine.EventSystems.PointerEventData.InputButton.Left || !this.enabled) 
            return;

        if((this.transitionType & TransitionType.SPRITE) != 0)
            this.spriteInfo.SetValue(this.targetGraphic, this.normalSprite);

        if((this.transitionType & TransitionType.COLOUR) != 0)
            this.targetGraphic.color = this.normalColour;

        if(this.cursorTexture != null)
            Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);

        this.OnMouseExit?.Invoke();
    }

    protected virtual void OnDisable()
    {
        if((this.transitionType & TransitionType.SPRITE) != 0 && this.disabledSprite != null)
            this.spriteInfo.SetValue(this.targetGraphic, this.disabledSprite);

        if((this.transitionType & TransitionType.COLOUR) != 0)
            this.targetGraphic.color = this.disabledColour;

        if(this.cursorTexture != null)
            Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
    }
};