﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Control all UIManager from this script.
/// </summary>
public class UIController : MonoBehaviour
{
    #if UNITY_EDITOR
    [MenuItem("GameObject/UI/UIController/UIManager", false, 10)]
	public static void CreateUIManager(MenuCommand command)
	{
		GameObject screen = new GameObject("[UIMANAGER] UIManagerscreen");
        GameObjectUtility.SetParentAndAlign(screen, command.context as GameObject);
        Undo.RegisterCreatedObjectUndo(screen, $"Create {screen.name}");
        Selection.activeObject = screen;

        UnityEngine.Canvas canvas = screen.AddComponent<UnityEngine.Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.planeDistance = 1.0f;
        
        UnityEngine.UI.CanvasScaler canvasScaler = screen.AddComponent<UnityEngine.UI.CanvasScaler>();
        canvasScaler.uiScaleMode = UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1280.0f, 720.0f);
        canvasScaler.matchWidthOrHeight = 0.5f;
        screen.AddComponent<UnityEngine.UI.GraphicRaycaster>();

        GameObject container = new GameObject("[HOLDER] Container");
        UnityEngine.UI.Image background = container.AddComponent<UnityEngine.UI.Image>();
        background.color = new Color32(34, 34, 34, 255);

        RectTransform rt = container.GetComponent<RectTransform>();
        rt.SetParent(screen.transform, false);
        rt.anchorMin = new Vector2(0.0f, 0.0f);
        rt.anchorMax = new Vector2(1.0f, 1.0f);
        rt.offsetMax = new Vector2(0, 0);
        rt.offsetMin = new Vector2(0, 0);
        rt.pivot = new Vector2(0.5f, 0.5f);
	}

    [MenuItem("GameObject/UI/UIController/UICamera", false, 10)]
	public static void CreateUICamera(MenuCommand command)
	{
		GameObject obj = new GameObject("[CAMERA] User Interface");
        GameObjectUtility.SetParentAndAlign(obj, (GameObject)command.context);
        Undo.RegisterCreatedObjectUndo(obj, $"Create UI Camera");
        Selection.activeObject = obj;

        obj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        obj.layer = 5;

        Camera camera = obj.AddComponent<Camera>();
        camera.clearFlags = CameraClearFlags.Depth;
        camera.useOcclusionCulling = false;
        camera.cullingMask = 1 << 5;
        camera.nearClipPlane = 0.0f;
        camera.farClipPlane = 1.0f;
        camera.orthographic = true;
        camera.allowMSAA = false;
        camera.allowHDR = false;
        camera.depth = 1.0f;

        obj.AddComponent<AudioListener>();
	}    
	#endif

    private static UIController instance;
    public static UIController singleton
    {
        get
        {
            if(instance == null) 
                instance = FindObjectOfType<UIController>();

            return instance;
        }
    }

    public static System.Action<UIManager> OnScreenChanged;

    [SerializeField]private UIManager firstUIManager = default;
    [SerializeField]private Camera uiCamera = default;
    [SerializeField]private Vector2 targetResolution = default;
    [SerializeField]private bool keepHistory = true;
    [SerializeField]private uint maxHistoryCount = 4;

    private UIManager[] screens = default;
    
    private System.Collections.Generic.List<System.Type> history = default;
    /// <summary>
    /// List of the previous shown UIManagers.
    /// </summary>
    public System.Collections.Generic.List<System.Type> History{get=> this.history;}

    private void Awake()=>
        this.UpdateManagers();

    private void Start()
    {
        if(this.keepHistory)
            this.history = new System.Collections.Generic.List<System.Type>();

        if(this.firstUIManager != null)
            this.GoToScreen(this.firstUIManager.GetType(), true);
    }

    /// <summary>
    /// Collect all the managers on the UIController gameobject.
    /// </summary>
    /// <returns>An array of current UIManagers</returns>
    public UIManager[] UpdateManagers()
    {
        if(this.uiCamera != null)
        {
            this.uiCamera.clearFlags = CameraClearFlags.Depth;
            this.uiCamera.cullingMask = 1 << 5;
            this.uiCamera.orthographic = true;
            this.uiCamera.depth = 1.0f;
        }

        this.screens = this.gameObject.GetComponents<UIManager>();
        if(this.screens is null)
            this.screens = new UIManager[0];

        int len = this.screens.Length;
        for(int i = 0; i < len; i++)
            this.screens[i].Setup(this.uiCamera, this.targetResolution);
        
        return this.screens;
    }

    /// <summary>
    /// Go to screen from history list.
    /// </summary>
    /// <param name="stepSize">(Optional) Amount of screens to go back</param>
    /// <returns>False if you can't go back any further</returns>
    public bool GoBackInHistory(int stepSize = 1)
    {
        if(stepSize < 1 || stepSize > this.maxHistoryCount - 1)
        {
            Debug.LogWarning($"Cannot go back {stepSize} screens");
            return true;
        }

        int current = this.history.Count - 1;
        if(current is 0)
            return false;
            
        int index = current - stepSize;
        System.Type screen = this.history[index];
        this.history.RemoveRange(index, current - index);
        this.GoToScreen(screen);
        return true;
    }

    public void GoToScreen<T>(bool force = false)=> this.GoToScreen(typeof(T), force);

    /// <summary>
    /// Hide all screens except the given screen(string.Empty to hide all screens).
    /// </summary>
    /// <param name="screenType">The screentype to enable(UIManager.ToType)</param>
    public void GoToScreen(System.Type type, bool force = false)
    {
        if(this.screens == null)
            return;

        UIManager manager = null;
        int len = this.screens.Length;

        for(int i = 0; i < len; i++)
        {
            if(this.screens[i].IgnoreController && !force)
                continue;
            
            if(type.Equals(this.screens[i].GetType()))
                manager = this.screens[i];
            
            this.screens[i].Show(false);
        }

        if(manager != null)
        {
            if(this.keepHistory)
            {
                if(this.history is null)
                    this.history = new System.Collections.Generic.List<System.Type>();

                this.history.Add(type);
                if(this.history.Count > this.maxHistoryCount)
                    this.history.RemoveAt(0);
            }
            manager.Show(true);
        }

        OnScreenChanged?.Invoke(manager);
    }
};