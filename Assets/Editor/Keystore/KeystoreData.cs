﻿using UnityEngine;

public class KeystoreData : ScriptableObject 
{
	public string keystore = "";
	public string keystorePass = "";
	
	public string keystoreAliasName = "";
	public string keystoreAliasPass = "";
}
