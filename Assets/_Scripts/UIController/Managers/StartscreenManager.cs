﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UIManager to introduce the player to the game.
/// </summary>
public class StartscreenManager : UIManager
{
	public static StartscreenManager singleton;

	[SerializeField]private UIButton startButton = default;
	[SerializeField]private UIButton creditsButton = default;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this) 
			Destroy(this);
		singleton = this;
	}   
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying) 
			return;

		this.startButton.OnClick = ()=> 
			UIController.singleton.GoToScreen<LevelselectionscreenManager>();

		this.creditsButton.OnClick = ()=>
			UIController.singleton.GoToScreen<CreditsscreenManager>();
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying) 
			return;    

		this.startButton.OnClick = null;
		this.creditsButton.OnClick = null;
	}
};