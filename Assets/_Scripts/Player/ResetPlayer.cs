using UnityEngine;

/// <summary>
/// Responsible for reseting the player to the beginstate of a level.
/// </summary>
[RequireComponent(typeof(BikeMovement))]
public class ResetPlayer : MonoBehaviour
{
    public static ResetPlayer singleton;

    [SerializeField]private float resetDelayInSeconds = default;
    [SerializeField]private Vector3 startPosition = default;
    /// <summary>
    /// Get/Set the starting position of the current level
    /// </summary>
    public Vector3 StartPosition{get=> this.startPosition; set=> this.startPosition = value;}

    private BikeMovement movementScript;
    
    private void Awake()
    {
        if(singleton != null && singleton != this)
            Destroy(this);
        singleton = this;

        this.movementScript = this.GetComponent<BikeMovement>();
    }

    #if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
            this.ResetMovementAndPosition(false);
    }
    #endif

    /// <summary>
    /// Reset the player to the current starting position and rotation and reset the movement script.
    /// </summary>
    /// <param name="useDelay">Should the method use a delay before resetting</param>
    public void ResetMovementAndPosition(bool useDelay = true, bool force = false)
    {
        if(DATA.LEVEL_COMPLETED && !force)
            return;
            
        void Reset()
        {
            this.movementScript.ResetMovement();
            this.movementScript.transform.localEulerAngles = new Vector3(0.0f, -90.0f, 0.0f);
            this.movementScript.transform.localPosition = this.startPosition;
        };

        System.Collections.IEnumerator WaitForReset()
        {
            yield return new WaitForSeconds(this.resetDelayInSeconds);
            Reset();
        };

        if(useDelay)
            StartCoroutine(WaitForReset());
        else 
            Reset();
    }
};