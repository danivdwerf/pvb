using UnityEngine;

[RequireComponent(typeof(Animator))]
/// <summary>
/// Class to play the animation on the player.
/// </summary>
public class PlayerAnimationHandler : MonoBehaviour
{
    #region Animatior
    private Animator playerAnimator;
    private int velocityID;
    #endregion

    private BikeMovement movementScript;

    private void Start()
    {
        this.playerAnimator = this.GetComponent<Animator>();
        this.velocityID = Animator.StringToHash("velocity");
        this.movementScript = this.GetComponent<BikeMovement>();
    }

    private void Update()
    {
        float velocity = this.movementScript.Velocity;
        this.playerAnimator.SetFloat(this.velocityID, velocity);
        this.playerAnimator.speed = velocity.IsBetween(-0.0001f, 0.0001f) ? 1.0f : Mathf.Abs(velocity * 40);
    }
};