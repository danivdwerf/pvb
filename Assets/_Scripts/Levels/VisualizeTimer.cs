﻿using UnityEngine;

/// <summary>
/// Class to show the time as 3D text.
/// </summary>
public class VisualizeTimer : MonoBehaviour
{
    private TextMesh timerText;

    private void Start()=>
        this.timerText = gameObject.GetComponent<TextMesh>();
    
    private void OnEnable()
    {
        if(Timer.singleton == null)
            return;
        
        if(this.timerText == null)
            this.timerText = gameObject.GetComponent<TextMesh>();
        
        this.timerText.text = "00:00:00";
        Timer.OnTimerTick += this.UpdateTimer;
    }

    private void UpdateTimer(float ticks)
    {
        (float minutes, float seconds, float milliseconds) = ticks.ToMinutesSecondsMiliseconds();
        string time = $"{minutes.ToString("00")}:{seconds.ToString("00")}:{milliseconds}";
        this.timerText.text = time;
    }

    private void OnDisable()=>
        Timer.OnTimerTick -= this.UpdateTimer;
};