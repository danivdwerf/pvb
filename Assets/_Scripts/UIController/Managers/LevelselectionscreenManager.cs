﻿using UnityEngine;
using UnityEngine.UI;

using System.Linq;

/// <summary>
/// Class responsible for showing and letting the user select a level.
/// </summary>
public class LevelselectionscreenManager : UIManager
{
	public static LevelselectionscreenManager singleton;

	/// <summary>
	/// Action called when a button is clicked.
	/// </summary>
	public static System.Action<uint> OnLevelSelected;

	[SerializeField]private UIButton close = default;
	[SerializeField]private Text levelName = default;

	[Header("Level list")]
	[SerializeField]private RectTransform list = default;
	[SerializeField]private UIButton itemPrefab = default;

	private Vector2 listRange;
	private ScrollItem activeItem;
	private Vector2 previousPosition;
	private Vector2 itemSize;
	private bool isDown;
	private bool canDrag;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}

	private void Start()
	{	
		this.list.ClearChildren();

		LevelData[] levels = DATA.LEVEL_DATA;
		int len = levels.Length;
		for(int i = 0; i < len; i++)
		{
			UIButton button = GameObject.Instantiate<UIButton>(this.itemPrefab);
			button.transform.SetParent(this.list, false);
			button.SetSprite(levels[i].LevelThumbnail);

			int index = i;
			button.OnClick = ()=> 
			{
				if(!button.transform.Equals(this.activeItem.transform))
					return;
				
				OnLevelSelected?.Invoke(levels[index].UID);
			};
		}

		this.itemSize = ((RectTransform)this.itemPrefab.transform).sizeDelta;
		this.listRange.y = (2040.0f / 2.0f) - this.itemSize.x / 2.0f;
		this.listRange.x = this.listRange.y - ((this.itemSize.x - 100) * (len - 1));
	}
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying)
			return;

		this.close.OnClick += ()=>
			UIController.singleton.GoToScreen<StartscreenManager>();

		UIButton[] buttons = this.list.GetComponentsInChildren<UIButton>();
		int len = buttons.Length;
		for(int i = 0; i < len; i++)
		{
			System.Collections.Generic.Dictionary<int, object[]> results = SQLite.singleton.Select($"SELECT * FROM `score` WHERE `levelID` = {DATA.LEVEL_DATA[i].UID}");
			
			if(results.Count < 1)
			{
				buttons[i].Text = "-- : -- : --";
				continue;
			}

			(float minutes, float seconds, float milliseconds) = ((float)results[0][1]).ToMinutesSecondsMiliseconds();
			buttons[i].Text = $"{minutes.ToString("00")} : {seconds.ToString("00")} : {milliseconds.ToString("00")}";
		}
		
		#region Select first item
		this.levelName.text = DATA.LEVEL_DATA[0].LevelName;
		this.activeItem = this.list.GetComponentInChildren<ScrollItem>();
		Vector2 startPos = this.list.anchoredPosition;
		startPos.x = this.listRange.y;
		this.list.anchoredPosition = startPos;
		this.activeItem.Activate(true);
		#endregion

		this.canDrag = true;
	}

	private void Update()
	{
		if(!this.IsEnabled)
			return;
		
		// Add mouse support for debugging in the editor
		#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0))
			this.OnTouchStart(Input.mousePosition);

		if(this.isDown)
			this.OnTouchUpdate(Input.mousePosition);

		if(Input.GetMouseButtonUp(0))
			this.OnTouchEnd();
		#endif

		if(!Input.touchCount.Equals(1))
			return;
		
		Touch touch = Input.GetTouch(0);	
		if(touch.phase.Equals(TouchPhase.Began))
			this.OnTouchStart(touch.position);
		else if(touch.phase.Equals(TouchPhase.Stationary) || touch.phase.Equals(TouchPhase.Moved))
			this.OnTouchUpdate(touch.position);
		else if(touch.phase.Equals(TouchPhase.Ended) || touch.phase.Equals(TouchPhase.Canceled))
			this.OnTouchEnd();
	}

	private void OnTouchStart(Vector2 position)
	{
		if(this.isDown || !this.canDrag)
			return;
		
		this.isDown = true;
		this.canDrag = false;
		this.previousPosition = position;
	}

	private void OnTouchUpdate(Vector2 position)
	{
		if(!this.isDown)
			return;
		
		float previousPosition = this.previousPosition.x - position.x;
		Vector2 newPos = this.list.anchoredPosition;
		newPos.x -= previousPosition;

		if(newPos.x < this.listRange.x)
			newPos.x = this.listRange.x;
		else if(newPos.x > this.listRange.y)
			newPos.x = this.listRange.y;
		
		this.list.anchoredPosition = newPos;
		this.previousPosition = position;
	}

	private void OnTouchEnd()
	{
		if(!this.isDown)
			return;

		(float active, int levelIndex) = this.GetActiveXAndIndex();
		this.levelName.text = DATA.LEVEL_DATA[levelIndex].LevelName;
		this.StartCoroutine(this.LerpToPosition(this.list.position - new Vector3(active, 0.0f, 0.0f)));

		this.previousPosition = default;
		this.isDown = false;
	}

	private System.Collections.IEnumerator LerpToPosition(Vector3 target)
	{
		float value = 0.0f;
		while(value < 1.0f)
		{
			this.list.position = Vector3.Lerp(this.list.position, target, value);
			value += 0.05f;
			yield return new WaitForEndOfFrame();
		}
		this.list.position = target;
		this.canDrag = true;
	}

	private (float, int) GetActiveXAndIndex()
	{
		ScrollItem[] items = this.list.GetComponentsInChildren<ScrollItem>();

		ScrollItem activeItem = items.OrderByDescending(item => item.transform.localScale.x).First();
		int levelIndex = System.Array.IndexOf(items, activeItem);
		if(levelIndex < 0)
			levelIndex = 0;

		if(!this.activeItem.Equals(activeItem))
		{
			this.activeItem.Activate(false);
			this.activeItem = activeItem;
			this.activeItem.Activate(true);
		}

		return(activeItem.transform.position.x, levelIndex);
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying)
			return;

		this.close.OnClick = null;
		this.levelName.text = string.Empty;
		this.activeItem = null;
		this.previousPosition = default;
		this.isDown = false;
		this.canDrag = true;
	}
};