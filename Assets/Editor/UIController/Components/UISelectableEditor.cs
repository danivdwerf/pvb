﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UISelectable), true)]
[CanEditMultipleObjects]
public class UISelectableEditor : Editor
{
	#region Transitions
	protected SerializedProperty transitionType;

	#region Colours
	protected SerializedProperty normalColour;
	protected SerializedProperty hoverColour;
	protected SerializedProperty disabledColour;
	#endregion

	#region sprites
	protected SerializedProperty hoverSprite;
	protected SerializedProperty disabledSprite;
	#endregion
	#endregion

    protected SerializedProperty cursorTexture;

	protected virtual void OnEnable()
	{
		this.transitionType = this.serializedObject.FindProperty("transitionType");

		this.normalColour = this.serializedObject.FindProperty("normalColour");
		this.hoverColour = this.serializedObject.FindProperty("hoverColour");
		this.disabledColour = this.serializedObject.FindProperty("disabledColour");

		this.hoverSprite = this.serializedObject.FindProperty("hoverSprite");
		this.disabledSprite = this.serializedObject.FindProperty("disabledSprite");

		this.cursorTexture = this.serializedObject.FindProperty("cursorTexture");
	}

	public override void OnInspectorGUI()
    {
        this.serializedObject.Update();

		EditorGUILayout.Space();
		this.CreateTransitions();

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(this.cursorTexture);

        this.serializedObject.ApplyModifiedProperties();
    }

	protected virtual void CreateTransitions()
	{
		EditorGUILayout.LabelField("Transition", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(this.transitionType);
		if((this.transitionType.intValue & (int)UISelectable.TransitionType.COLOUR) != 0)
		{
			EditorGUILayout.PropertyField(this.normalColour);
			EditorGUILayout.PropertyField(this.hoverColour);
			EditorGUILayout.PropertyField(this.disabledColour);
		}

		if((this.transitionType.intValue & (int)UISelectable.TransitionType.SPRITE) != 0)
		{
			EditorGUILayout.PropertyField(this.hoverSprite);
			EditorGUILayout.PropertyField(this.disabledSprite);
		}
	}
};