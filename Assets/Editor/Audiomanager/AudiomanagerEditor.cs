﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AudioManager), true)]
[CanEditMultipleObjects]
public class AudioManagerEditor : Editor
{
    private SerializedProperty audioClips;
    private SerializedProperty audioTags;
    private System.Collections.Generic.List<bool> status;

    private void OnEnable()
    {
        this.audioClips = this.serializedObject.FindProperty("audioClips");
        this.audioTags = this.serializedObject.FindProperty("audioTags");
        
        if(this.status is null)
            this.status = new System.Collections.Generic.List<bool>();

        if(this.audioTags.arraySize is 0)
            this.audioTags.InsertArrayElementAtIndex(0);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.Space();
        this.ListItems();

        if(GUILayout.Button("Add clip"))
            (target as AudioManager).AudioClips.Add(new AudioClip());

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        this.ListTags();

        Editor.DrawPropertiesExcluding(this.serializedObject, "m_Script", "audioClips", "audioTags");
        serializedObject.ApplyModifiedProperties();
    }

    private void ListItems()
    {
        AudioManager manager = target as AudioManager;
        if(manager.AudioClips is null)
            return;
        
        int len = manager.AudioClips.Count;
        for (int i = 0; i < len; i++)
        {
            AudioClip clip = manager.AudioClips[i];
            if(clip is null)
                continue;

            if(i >= this.status.Count)
                this.status.Add(false);
            
            this.status[i] = EditorGUILayout.Foldout(this.status[i], clip.Name, true);
            if(this.status[i])
            {
                if(GUILayout.Button("delete clip"))
                {
                    manager.AudioClips.Remove(clip);
                    return;
                }

                clip.Name = EditorGUILayout.TextField("Name:", clip.Name);
                clip.Tag = EditorGUILayout.Popup(clip.Tag, manager.AudioTags.ToArray());
                if(clip.Tag >= manager.AudioTags.Count)
                    clip.Tag = 0;

                clip.Clip = (UnityEngine.AudioClip)EditorGUILayout.ObjectField("Audioclip:", clip.Clip, typeof(UnityEngine.AudioClip), true);
                clip.Volume = EditorGUILayout.Slider("Volume:", clip.Volume, 0, 1);
                clip.Pitch = EditorGUILayout.Slider("Pitch:", clip.Pitch, -3, 3);
                clip.Panning = EditorGUILayout.Slider("Panning:", clip.Panning, -1, 1);

                clip.Loop = EditorGUILayout.Toggle("Loop:", clip.Loop);
                clip.PlayFromStart = EditorGUILayout.Toggle("Play from Start:", clip.PlayFromStart);

                clip.SpatialBlend = EditorGUILayout.Slider("Spatial blend:", clip.SpatialBlend, 0, 1);
                if(clip.SpatialBlend > 0)
                {
                    clip.MinDistance = EditorGUILayout.FloatField("Min distance:", clip.MinDistance);
                    clip.MaxDistance = EditorGUILayout.FloatField("Max distance:", clip.MaxDistance);
                    clip.SourcePosition = (Transform)EditorGUILayout.ObjectField("Position:", clip.SourcePosition, typeof(Transform), true);
                }

                if (!Selection.activeTransform)
                    this.status[i] = false;
            }            
        }
    }

    private void ListTags()
    {
        EditorGUILayout.LabelField("Audio tags:", EditorStyles.boldLabel);

        int len = this.audioTags.arraySize;
        for(int i = 0; i < len; i++)
        {
            EditorGUILayout.BeginHorizontal();
            (target as AudioManager).AudioTags[i] = EditorGUILayout.TextField($"Tag {i}", this.audioTags.GetArrayElementAtIndex(i).stringValue);
            if(GUILayout.Button("x"))
            {
                this.audioTags.DeleteArrayElementAtIndex(i);
                return;
            }
            
            EditorGUILayout.EndHorizontal();
        }

        if(GUILayout.Button("Add tag"))
            this.audioTags.InsertArrayElementAtIndex(len is 0 ? 0 : len - 1);
    }
};