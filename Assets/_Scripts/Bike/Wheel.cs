using UnityEngine;

/// <summary>
/// Represents a Wheel to use with the PlayerMovement class.
/// </summary>
public class Wheel : MonoBehaviour
{
    [Header("Ground collision")]
    [SerializeField]private float maxGroundedDistance = 0.1f;
    [SerializeField]private bool drawGroundedDetection = default;

    [Header("Rotating")]
    [SerializeField]private float rotationMultiplier = -500.0f;
    [SerializeField]private Vector3 rotationAxis = Vector3.left;

    private bool isGrounded;
    /// <summary>
    /// Get if the the wheel is touching the ground based on the Wheel.maxGroundedDistance value
    /// </summary>
    public bool IsGrounded{get=> this.isGrounded;}
    
    private void Update()=> 
        this.isGrounded = Physics.Raycast(this.transform.position, new Vector3(0.0f, this.maxGroundedDistance, 0.0f));

    /// <summary>
    /// Rotate the wheel based on the given velocity
    /// </summary>
    /// <param name="velocity">The velocity to use</param>
    public void RotateWithVelocity(float velocity)
    {
        if(!this.isGrounded)
            return;

        this.transform.Rotate(this.rotationAxis, velocity * this.rotationMultiplier);
    }

    private void OnDrawGizmos()
    {
        if(!this.drawGroundedDetection)
            return;
        
        Gizmos.color = Color.red;
        Gizmos.DrawLine(this.transform.position, this.transform.position + new Vector3(0.0f, this.maxGroundedDistance, 0.0f));
    }
};