using UnityEditor;
using UnityEngine;

public class KeystoreEditor : EditorWindow
{
    private string keystore;
    private string keystorePass;
    private string aliasName;
    private string aliasPass;

    [MenuItem("Window/Parkers/Keystore")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(KeystoreEditor));
    }

    private void Awake()
    {
        this.keystore = PlayerSettings.Android.keystoreName;
        this.keystorePass = PlayerSettings.Android.keystorePass;
        this.aliasName = PlayerSettings.Android.keyaliasName;
        this.aliasPass = PlayerSettings.Android.keyaliasPass;
    }

    private void SelectKeystore()
    {
        string path = EditorUtility.OpenFilePanel("Select a file", "", "");
        if(string.IsNullOrEmpty(path)) return;
        this.keystore = path;
    }

    private void OnGUI()
    {
        GUILayout.Label("Keystore settings", EditorStyles.boldLabel);

        #region Select keystore
        GUILayout.Label(string.Format("Current path: {0}", PlayerSettings.Android.keystoreName), EditorStyles.label);
        if(GUILayout.Button("Select keystore")) this.SelectKeystore();
        GUILayout.Space(10);
        #endregion

        #region Signin
        GUILayout.Label("Credentials");
        this.keystorePass = EditorGUILayout.PasswordField("Keystore password:", this.keystorePass);
        this.aliasName = EditorGUILayout.TextField("Alias name:", this.aliasName);
        this.aliasPass = EditorGUILayout.PasswordField("Alias password:", this.aliasPass);

        if(GUILayout.Button("Update")) KeystoreLoader.SaveValues(this.keystore, this.keystorePass, this.aliasName, this.aliasPass);
        #endregion
    }
}

[InitializeOnLoad]
public class KeystoreLoader
{
    private static KeystoreData data;
    private static string path = "Assets/Resources/KeystoreData.asset";

    static KeystoreLoader()
    {
        if(Application.isPlaying) return;

        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));

        data = (KeystoreData)Resources.Load("KeystoreData") as KeystoreData;
        if(data == null) data = ScriptableObject.CreateInstance<KeystoreData>();
        UpdateSettings(data.keystore, data.keystorePass, data.keystoreAliasName, data.keystoreAliasPass);
    }

    private static void UpdateSettings(string keystore, string pass, string alias, string aliasPass)
    {
        PlayerSettings.Android.keystoreName = keystore;
        PlayerSettings.Android.keystorePass = pass;
        PlayerSettings.Android.keyaliasName = alias;
        PlayerSettings.Android.keyaliasPass = aliasPass;
    }

    public static void SaveValues(string keystore, string pass, string alias, string aliasPass)
    {
        if(Application.isPlaying) return;

        if(data == null) 
        {
            data = Editor.CreateInstance<KeystoreData>();
            AssetDatabase.CreateAsset(data, path);
        }

        data.keystore = keystore;
        data.keystorePass = pass;
        data.keystoreAliasName = alias;
        data.keystoreAliasPass = aliasPass;

        UpdateSettings(keystore, pass, alias, aliasPass);

        Debug.Log(string.Format("Saving keystore {0}.", path));
        EditorUtility.SetDirty(data);   
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
};