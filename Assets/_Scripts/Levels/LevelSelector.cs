﻿using UnityEngine;

/// <summary>
/// Manager containing levels and functionality to load and unload them.
/// </summary>
public class LevelSelector : MonoBehaviour
{
    [SerializeField]private LevelData[] levels = default;
    [SerializeField]private Transform levelParent = default;
    [SerializeField]private GameObject playerPrefab = default;
    [SerializeField]private float playerObjectSpawnDelay = 0.1f;
    [SerializeField]private TrackableEventHandler trackHandler = default;
    [SerializeField]private PlaneDetectionImage planeDetection = default;

    private GameObject player = default;
    private GameObject[] levelGameobjects;

    private void Awake()
    {
        this.levelGameobjects = new GameObject[this.levels.Length];
        int len = this.levels.Length;
        for(uint i = 0; i < len; i++)
        {
            GameObject levelObject = Instantiate(this.levels[i].LevelPrefab);
            levelObject.transform.SetParent(this.levelParent, false);
            levelObject.SetActive(false);
            
            this.levelGameobjects[i] = levelObject;
        }
        DATA.LEVEL_DATA = this.levels;

        this.player = Instantiate(playerPrefab);
        this.player.transform.SetParent(levelParent);
        this.player.SetActive(false);
    }

    private void OnEnable()
    {
        LevelselectionscreenManager.OnLevelSelected += this.LoadLevel;
        ScorescreenManager.OnClosePressed += this.UnloadLevel;
    }
        
    private void OnDisable()
    {
        LevelselectionscreenManager.OnLevelSelected -= this.LoadLevel;
        ScorescreenManager.OnClosePressed -= this.UnloadLevel;
    }
        
    private void LoadLevel(uint levelID)
    {
        DATA.LEVEL_COMPLETED = false;
        int len = this.levels.Length;
        for(int i = 0; i < len; i++)
        { 
            if(!this.levels[i].UID.Equals(levelID))
            {
                this.levelGameobjects[i].SetActive(false);
                continue;   
            }

            this.levelGameobjects[i].SetActive(true);
            DATA.CURRENT_LEVEL = this.levels[i].UID;
            this.player.transform.SetParent(this.levelGameobjects[i].transform);
        }

        UIController.singleton.GoToScreen<PopupscreenManager>();  
        this.planeDetection.ShouldLookForTarget = true;
        this.planeDetection.HasFoundTarget = false;

        System.Collections.IEnumerator SpawnDelay()
        {
            yield return new WaitForSeconds(this.playerObjectSpawnDelay);
            this.player.SetActive(true);
            ResetPlayer.singleton.ResetMovementAndPosition(false);
        };

        this.StartCoroutine(SpawnDelay());        
    }

    /// <summary>
    /// Make sure the level is unloaded and a new level can be started.
    /// </summary>
    public void UnloadLevel()
    {
        ResetPlayer.singleton.ResetMovementAndPosition(false, true);
        this.player.SetActive(false);
        int len = this.levelGameobjects.Length;
        for(int i = 0; i < len; i++)
            this.levelGameobjects[i].SetActive(false);
    
        UIController.singleton.GoToScreen<LevelselectionscreenManager>();
        this.trackHandler.EnableComponents(false);
        this.planeDetection.ShouldLookForTarget = false;
    }
};
