using UnityEngine;

/// <summary>
/// Handler that improves Vuforia's DefaultTrackableEventHandler
/// </summary>
public class TrackableEventHandler : DefaultTrackableEventHandler
{
    [SerializeField]private bool disableRenderers = default;
    [SerializeField]private bool disableColliders = default;
    [SerializeField]private bool disableCanvas = default;

    protected override void OnTrackingFound()=>
        this.EnableComponents(true);

    protected override void OnTrackingLost()=>
        this.EnableComponents(false);

    /// <summary>
    /// Enable/Disable the selected components
    /// </summary>
    /// <param name="enable">True if the components should be activated</param>
    public void EnableComponents(bool enable)
    {
        if(this.disableRenderers)
        {
            Renderer[] rendererComponents = this.GetComponentsInChildren<Renderer>(true);
            int len = rendererComponents.Length;
            for(int i = 0; i < len; i++)
                rendererComponents[i].enabled = enable;
        }

        if(this.disableColliders)
        {
            Collider[] colliderComponents = this.GetComponentsInChildren<Collider>(true);
            int len = colliderComponents.Length;
            for(int i = 0; i < len; i++)
                colliderComponents[i].enabled = enable;
        }

        if(this.disableCanvas)
        {
            Canvas[] canvasComponents = this.GetComponentsInChildren<Canvas>(true);
            int len = canvasComponents.Length;
            for(int i = 0; i < len; i++)
                canvasComponents[i].enabled = enable;
        }
    }
};