using UnityEngine;

/// <summary>
/// Class to position the level.
/// </summary>
public class RepositionLevel : DefaultTrackableEventHandler
{
    [SerializeField]private Vuforia.ContentPositioningBehaviour positioningBehaviour = default;

    private bool levelActive;
    /// <summary>
    /// Is there currently a level active?
    /// </summary>
    /// <value></value>
    public bool LevelActive{get=> this.levelActive; set=> this.levelActive = value;}

    /// <summary>
    /// Position level to the given HitTestResult.
    /// </summary>
    /// <param name="hitTest">The HitTestResult to use</param>
    public void ReposLevel(Vuforia.HitTestResult hitTest)
    {
        if(!this.levelActive)
            return;
        
        this.positioningBehaviour.PositionContentAtPlaneAnchor(hitTest);
    }

    private void OnDisable()=>
        this.levelActive = false;
};