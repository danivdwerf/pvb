using UnityEngine;
using UnityEditor;

public class UIControllerWindow : EditorWindow
{
    [MenuItem("Window/Parkers/UIController")]
    public static void ShowWindow()=> EditorWindow.GetWindow(typeof(UIControllerWindow));

    private UIManager[] screens;
    private Vector2 scrollPos;

	private void OnEnable()
	{
        this.screens = UIController.singleton.UpdateManagers();
        this.scrollPos = default(Vector2);
	}

    private void OnGUI()
    {
        if(UnityEditor.BuildPipeline.isBuildingPlayer)
        {
            EditorGUILayout.LabelField("Waiting for build to finish...");
            return;
        }

        #region Managers
        if(this.screens.Length is 0) 
            EditorGUILayout.LabelField("No managers available yet");
        else 
            this.ListManagers();
        #endregion

        #region Refresh
        this.CreateRefresh();
        #endregion
    }

    private void ListManagers()
    {
        int len = this.screens.Length;
        this.scrollPos = GUILayout.BeginScrollView(this.scrollPos);
        for(int i = 0; i < len; i++)
        {
            UIManager manager = this.screens[i];
            if(manager is null) 
                continue;

            System.Type type = manager.GetType();
            EditorGUILayout.BeginHorizontal();

            if(CustomGUI.ClickableLabel(manager.ToType(), new Color32(34, 34, 34, 255)))
                Selection.activeGameObject = manager.Screen.gameObject;

            GUIStyle style = new GUIStyle(GUI.skin.button);

            style.normal.textColor = new Color32(255, 255, 255, 255);
            GUI.backgroundColor = new Color32(34, 34, 34, 255);
            style.fixedWidth = 50.0f;

            if(GUILayout.Button("go to", style)) 
                UIController.singleton.GoToScreen(type, true);

            bool isEnabled = manager.IsEnabled;
            if(GUILayout.Button(isEnabled ? "disable" : "enable", style))
                manager.Show(!isEnabled);
                
            EditorGUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
    }

    private void CreateRefresh()
    {
        GUIStyle style = new GUIStyle(GUI.skin.button);
        style.normal.textColor = new Color32(255, 255, 255, 255);
        style.fixedWidth = 80.0f;
        style.fixedHeight = 25.0f;

        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if(GUILayout.Button("Refresh", style)) 
            this.screens = UIController.singleton.UpdateManagers();

        GUILayout.EndHorizontal();
    }
};