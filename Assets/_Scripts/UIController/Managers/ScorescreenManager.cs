﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UIManager to show the player the score.
/// </summary>
public class ScorescreenManager : UIManager
{
	public static ScorescreenManager singleton;
	/// <summary>
	/// Called when player pressed the close button.
	/// </summary>
	public static System.Action OnClosePressed;

	[SerializeField]private UIButton closeButton = default;
	[SerializeField]private Text scoreText = default; 

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}   
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying) 
			return;

		this.closeButton.OnClick = ()=> 
			OnClosePressed?.Invoke();
	}

	/// <summary>
	/// Set the score to the given value and show screen
	/// </summary>
	/// <param name="time">The score to show</param>
	public void ShowScore(float time)
	{
		(float minutes, float seconds, float milliseconds) = time.ToMinutesSecondsMiliseconds();
		this.scoreText.text = $"{minutes.ToString("00")}:{seconds.ToString("00")}:{milliseconds}";
		this.Show(true);
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying) 
			return;    

		this.closeButton.OnClick = null;
	}
};