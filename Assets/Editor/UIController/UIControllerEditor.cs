﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIController), true)]
[CanEditMultipleObjects]
public class UIControllerEditor : Editor
{
	private SerializedProperty firstUIManager;
    private SerializedProperty uiCamera;
    private SerializedProperty targetResolution;
    private SerializedProperty keepHistory;
    private SerializedProperty maxHistoryCount;

    private bool showHistory;

	private void OnEnable()
	{
		this.firstUIManager = this.serializedObject.FindProperty("firstUIManager");
		this.uiCamera = this.serializedObject.FindProperty("uiCamera");
		this.targetResolution = this.serializedObject.FindProperty("targetResolution");
		this.keepHistory = this.serializedObject.FindProperty("keepHistory");
		this.maxHistoryCount = this.serializedObject.FindProperty("maxHistoryCount");
	}

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        UIController controller = (UIController)target;

        #region Properties
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("UIController:", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(this.firstUIManager);
        EditorGUILayout.PropertyField(this.uiCamera);
        EditorGUILayout.PropertyField(this.targetResolution);
        EditorGUILayout.PropertyField(this.keepHistory);
        if(this.keepHistory.boolValue)
            EditorGUILayout.PropertyField(this.maxHistoryCount);
        #endregion

        #region History
        if(Application.isPlaying)
        {
            this.showHistory = EditorGUILayout.Foldout(this.showHistory, "History", true);

            if(this.showHistory)
            {
                int len = controller.History.Count;
                for(int i = 0; i < len; i++)
                {
                    string extra = "";
                    if(i.Equals(len - 1))
                        extra = "(current)";
                    else if(i.Equals(0))
                        extra = "(oldest)";
                    
                    EditorGUILayout.LabelField($"{i}: {controller.History[i]}{extra}");
                }
            }
        }
        #endregion
        
        serializedObject.ApplyModifiedProperties();
    }
};