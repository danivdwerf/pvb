# Wegwezen

# Introduction 
Dit is de codebase van groep 3 voor het eindexamen 2019 op Mediacollege. Het project heet W&W: Wegwezen en is een verzamel actie voor het merk W&W.

## Developers
* Danilo Beumans
* Jelmer de Voogt
* Dani van der Werf

## Artists
* Gideon Buijs
* Vincent de Böck
* Kasper Hoekzema
* Rolf van den Berg
* Dirk jan ter Brugge
* Lucila Ouweneel Rozemeijer