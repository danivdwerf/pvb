﻿using UnityEngine;

/// <summary>
/// Scriptableobject containing level data
/// </summary>
[CreateAssetMenu(fileName="Level Data", menuName="Level")]
public class LevelData : ScriptableObject
{
    /// <summary>
    /// Unique ID to identify the level without string comparison.
    /// </summary>
    public uint UID;

    /// <summary>
    /// The name to display.
    /// </summary>
    public string LevelName;

    /// <summary>
    /// The actual level gameobject.
    /// </summary>
    public GameObject LevelPrefab;

    /// <summary>
    /// The image to display
    /// </summary>
    public Sprite LevelThumbnail;

    public override string ToString()=>
        $"[Level({this.UID})] \"{this.LevelName}\"";
};