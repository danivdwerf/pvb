﻿using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// UIManager showing information about who made the game.
/// </summary>
public class CreditsscreenManager : UIManager
{
	public static CreditsscreenManager singleton;
	[SerializeField]private UIButton close;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}   
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying)
			return;

		this.close.OnClick = ()=>
			UIController.singleton.GoToScreen<StartscreenManager>();
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying)
			return;

		this.close.OnClick = null;
	}
};