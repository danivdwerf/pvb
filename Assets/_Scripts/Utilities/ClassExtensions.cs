﻿/// <summary>
/// Utility class with class extensions
/// </summary>
public static class ClassExtensions
{
    #region Float
    /// <summary>
    /// Check if this value is between the given minimal and maximal value
    /// </summary>
    /// <param name="min">Minimal value(inclusive)</param>
    /// <param name="max">Maximal value(inclusive)</param>
    /// <returns>True if value is between these values</returns>
    public static bool IsBetween(this float value, float min, float max)=> 
        value >= min && value <= max;
    #endregion

    #region Transform
    /// <summary>
    /// Remove all children of a Transform
    /// </summary>
    /// <param name="mustContain">Only remove children that contain this string in their name</param>
    public static void ClearChildren(this UnityEngine.Transform orig, string mustContain = null)
    {
        bool checkName = !string.IsNullOrEmpty(mustContain);
        
        int len = orig.childCount-1;
        for(int i = len; i >= 0; i--)
        {
            UnityEngine.Transform child = orig.GetChild(i);
            if(checkName && child.name.IndexOf(mustContain) < 0) 
                continue;
                
            UnityEngine.GameObject.Destroy(child.gameObject); 
        }
    }
    #endregion

    #region float
    public static (float, float, float) ToMinutesSecondsMiliseconds(this float time)
    {
        float minutes = time / 60;
        float seconds = time % 60;
        float milliseconds = (time * 100) % 100;
        return (UnityEngine.Mathf.Floor(minutes), UnityEngine.Mathf.Floor(seconds), UnityEngine.Mathf.Floor(milliseconds));
    }
    #endregion
};