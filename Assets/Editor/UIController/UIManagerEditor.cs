﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIManager), true)]
[CanEditMultipleObjects]
public class UIManagerEditor : Editor
{
	#region Screen
	private SerializedProperty screen;
    private SerializedProperty ignoreController;
    #endregion

	private void OnEnable()
	{
		this.screen = this.serializedObject.FindProperty("screen");
		this.ignoreController = this.serializedObject.FindProperty("ignoreController");
	}

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        UIManager manager = (UIManager)target;

        #region Screen
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("UIManager:", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(this.screen);
        EditorGUILayout.PropertyField(this.ignoreController);
        
        if(this.screen.objectReferenceValue != null) EditorGUILayout.LabelField(string.Format("status: {0} (focus inspector to update)", manager.IsEnabled ? "enabled" : "disabled"));

        EditorGUILayout.Space();
        EditorGUILayout.LabelField(string.Format("{0}:", target.GetType().ToString()), EditorStyles.boldLabel);
        #endregion

        Editor.DrawPropertiesExcluding(this.serializedObject, "screen", "ignoreController", "m_Script");
        
        serializedObject.ApplyModifiedProperties();
    }
};