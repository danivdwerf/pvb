using UnityEngine;

/// <summary>
/// Utility class to control the app's local databases
/// </summary>
public class SQLite : MonoBehaviour
{
    public static SQLite singleton;

    public enum FailReason{NONE, FILE_DOESNT_EXISTS, DATABASE_BUSY, FAILED_AFTER_MOVING_FILE};

    /// <summary>
    /// Callback when the current database file has changed
    /// </summary>
    public static System.Action OnDatabaseChange;

    private Mono.Data.Sqlite.SqliteConnection connection = null;
    /// <summary>
    /// The current connection with database.
    /// </summary>
    /// <value></value>
	public Mono.Data.Sqlite.SqliteConnection Connection{get => this.connection;}

    private bool isUpdatingDatabase;
    /// <summary>
    /// Returns if the database is currently being updated
    /// </summary>
    public bool IsUpdatingDatabase{get => this.isUpdatingDatabase;}
    
    private void Awake()
    {
        if(singleton != null && singleton != this) 
            Destroy(this);
        singleton = this;
    }
    
    /// <summary>
    /// Make connection to the database you want to use (Asynchrone)
    /// </summary>
    /// <param name="name">Path to the database file (relative to the Streaming Assets folder)</param>
    /// <param name="OnDatabaseSet">Callback when the database is successfully set</param>
    /// <param name="OnDatabaseFail">Callback when connecting failed</param>
    public void SetCurrentDatabase(string name, System.Action OnDatabaseSet = null, System.Action<FailReason> OnDatabaseFail = null)
	{
		if(this.isUpdatingDatabase)
        {
            OnDatabaseFail?.Invoke(FailReason.DATABASE_BUSY);
            return;
        }

		this.isUpdatingDatabase = true;
        System.Collections.IEnumerator SetDatabase()
        {
            string asset = $"{Application.streamingAssetsPath}/{name}";
            string path = asset;

            #if !UNITY_EDITOR 
            path = $"{Application.persistentDataPath}/{name}";
            #endif

            if(!System.IO.File.Exists(asset) && !System.IO.File.Exists(path))
            {
                OnDatabaseFail?.Invoke(FailReason.FILE_DOESNT_EXISTS);
                this.isUpdatingDatabase = false;
                yield break;
            }

            #if !UNITY_EDITOR
            #if UNITY_ANDROID
            if(!System.IO.File.Exists(path))
            {
                UnityEngine.Networking.UnityWebRequest www = new UnityEngine.Networking.UnityWebRequest(asset);
                yield return www.SendWebRequest();
                byte[] data = www.downloadHandler.data;

                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                System.IO.File.WriteAllBytes(path, data);
            }

            #elif UNITY_IOS
            if(!System.IO.File.Exists(path))
		    {
			    byte[] data = System.IO.File.ReadAllBytes(asset);
			    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path)); 
                System.IO.File.WriteAllBytes(path, data);
		    }
            #endif
            #endif
            
            string connectionPath = $"URI=file:{path}";
            Mono.Data.Sqlite.SqliteConnection connection = new Mono.Data.Sqlite.SqliteConnection(connectionPath);
            if(connection is null)
            {
                Debug.LogError($"SQLite.SetDatabase: {path} can't be found");
                this.isUpdatingDatabase = false;
                OnDatabaseFail?.Invoke(FailReason.FILE_DOESNT_EXISTS);
                yield break;
            }

            this.connection = connection;
            this.isUpdatingDatabase = false;
            OnDatabaseSet?.Invoke();
            OnDatabaseChange?.Invoke();
        }
		StartCoroutine(SetDatabase());
	}

    /// <summary>
    /// Create a database file if it doesn't exist already
    /// </summary>
    /// <param name="path">Path the file (relative to the Streaming Assets folder)</param>
    /// <param name="deleteIfExists">Delete the database file if it already exists</param>
    /// <param name="OnSucces">Callback when database is created</param>
    /// <param name="OnFail">Callback when failed to create database</param>
    public void CreateDatabase(string path, bool deleteIfExists = false, System.Action OnSucces = null, System.Action OnFail = null)
    {
        string asset = $"{Application.streamingAssetsPath}/{path}";
        #if !UNITY_EDITOR
        asset = $"{Application.persistentDataPath}/{path}";
        #endif

        if(System.IO.File.Exists(asset))
        {
            if(deleteIfExists.Equals(false))
                return;
            
            System.IO.File.Delete(asset);
        }

        try
        {
            Mono.Data.Sqlite.SqliteConnection.CreateFile(asset);
        }
        catch(System.Exception e)
        {
            Debug.LogError(e);
            OnFail?.Invoke();
        }
    }

    /// <summary>
    /// Create a table if it doesn't exists already.
    /// </summary>
    /// <param name="name">The name of the table</param>
    /// <param name="fields">The fields of the table</param>
    /// <param name="dropIfExists">If true, table with same name will be dropped before creating this one</param>
    public void CreateTable(string name, System.Collections.Generic.Dictionary<string, string> fields, bool dropIfExists = false)
	{
		if(this.connection is null)
		{
			Debug.LogError("SQLite.CreateTable: No active connection to database. Make sure you called SQLite.SetCurrentDatabase");
			return;
		}

        if(dropIfExists) 
            this.RunCommand($"DROP TABLE IF EXISTS `{name}`");

        int len = fields.Count;
		string[] keys = new string[len];
		fields.Keys.CopyTo(keys, 0);

		string query = $"CREATE TABLE IF NOT EXISTS `{name}`(";
		for(int i = 0; i < len; i++)
		{
			query += $"`{keys[i]}`";
			query += fields[keys[i]];
			if(!i.Equals(len - 1))
                query += ",";
		}
		query += ");";

        this.RunCommand(query);
	}

    /// <summary>
    /// Run the SELECT command to retrieve a table's contents
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    public System.Collections.Generic.Dictionary<int, object[]> Select(string query)
	{
		this.connection.Open();
		Mono.Data.Sqlite.SqliteCommand cmd = this.connection.CreateCommand();
		cmd.CommandType = System.Data.CommandType.Text;
		cmd.CommandText = query;

        System.Collections.Generic.Dictionary<int, object[]> response = new System.Collections.Generic.Dictionary<int, object[]>();

		Mono.Data.Sqlite.SqliteDataReader reader = cmd.ExecuteReader();
		int len = reader.FieldCount;
		int iteration = 0;
		while(reader.Read())
		{
			object[] data = new object[len];
			for(int i = 0; i < len; i++)
			{
				System.Type type = reader.GetFieldType(i);
				
				data[i] = null;
				if(type.Equals(typeof(System.String)))
                    data[i] = reader.GetString(i);
				else if(type.Equals(typeof(System.Int64)))
                    data[i] = reader.GetInt64(i);
				else if(type.Equals(typeof(System.Int32)))
                    data[i] = reader.GetInt32(i);
				else if(type.Equals(typeof(System.Int16)))
                    data[i] = reader.GetInt16(i);
                else if(type.Equals(typeof(System.Double)))
                    data[i] = (float)reader.GetDouble(i);
                else if(type.Equals(typeof(System.Decimal)))
                    data[i] = (float)reader.GetFloat(i);
			}
			response.Add(iteration, data);
			iteration++;
		}
        
		reader.Close();
		this.connection.Close();
		return response;
	}

    /// <summary>
    /// Run a SQL command on the database
    /// </summary>
    /// <param name="query">The command to run</param>
    public void RunCommand(string query)
	{
		if(this.connection is null)
		{
            Debug.LogError("SQLite.RunCommand: No active connection to database. Make sure you called SQLite.SetCurrentDatabase");
			return;
		}
		
		this.connection.Open();
		Mono.Data.Sqlite.SqliteCommand cmd = this.connection.CreateCommand();
		cmd.CommandType = System.Data.CommandType.Text;
		cmd.CommandText = query;
		cmd.ExecuteNonQuery();
		this.connection.Close();
    }
};