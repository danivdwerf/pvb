﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UIManager holding the controls the player can use while playing.
/// </summary>
public class HUDscreenManager : UIManager
{
	public static HUDscreenManager singleton; 

	#region Actions
	/// <summary>
	/// Called when player presses a direction
	/// </summary>
	public static System.Action<bool> OnDirectionDown;
	/// <summary>
	/// Called when player let's go the direction button
	/// </summary>
	public static System.Action OnDirectionUp;

	/// <summary>
	/// Called when player presses a lean button
	/// </summary>
	public static System.Action<bool> OnLeanDown;
	/// <summary>
	/// Called when player releases the lean button
	/// </summary>
	public static System.Action OnLeanUp;

	/// <summary>
	/// Called when player presses the recenter button
	/// </summary>
	public static System.Action OnRecenter;
	#endregion

	[Header("HUD")]
	[SerializeField]private UIButton forwardButton = default;
	[SerializeField]private UIButton backwardButton = default;
	[SerializeField]private UIButton leanForwardsButton = default;
	[SerializeField]private UIButton leanBackwardsButton = default;

	[Header("Menu")]
	[SerializeField]private GameObject blocker = default;
	[SerializeField]private AnimationCurve curve = default;
	[SerializeField]private UIButton pauseToggle = default;
	[SerializeField]private Sprite[] toggleSprites = default;
	[SerializeField]private RectTransform menuTransform = default;

	[Header("Menu items")]
	[SerializeField]private UIButton exitButton = default;  
	[SerializeField]private UIButton recenterButton = default;  
	[SerializeField]private UIButton helpButton = default;  

	[Space(10)]
	[SerializeField]private LevelSelector levelSelector = default;

	private bool isMenuActive;

	protected override void Awake()
	{
		base.Awake();
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;

		PopupscreenManager.OnClose += ()=> 
			UIController.singleton.GoToScreen(this.GetType());
	}

	private void Start()=>
		this.blocker.SetActive(false);
	
	protected override void OnScreenEnabled()
	{
		base.OnScreenEnabled();
		if(!Application.isPlaying)
			return;

		this.forwardButton.OnButtonDown = ()=> 
			OnDirectionDown?.Invoke(true);
		this.forwardButton.OnButtonUp = ()=> 
			OnDirectionUp?.Invoke();

		this.backwardButton.OnButtonDown = ()=> 
			OnDirectionDown?.Invoke(false);
		this.backwardButton.OnButtonUp = ()=> 
			OnDirectionUp?.Invoke();

		this.leanForwardsButton.OnButtonDown = ()=> 
			OnLeanDown?.Invoke(true);
		this.leanForwardsButton.OnButtonUp = ()=> 
			OnLeanUp?.Invoke();

		this.leanBackwardsButton.OnButtonDown = ()=> 
			OnLeanDown?.Invoke(false);
		this.leanBackwardsButton.OnButtonUp = ()=> 
			OnLeanUp?.Invoke();

		this.pauseToggle.OnClick = this.ToggleMenu;
	}

	private void ToggleMenu()
	{
		System.Action empty = null;
		this.isMenuActive = !this.isMenuActive;
		this.blocker.SetActive(this.isMenuActive);
		if(this.isMenuActive)
			Timer.singleton.PauseTimer();
		else 
			Timer.singleton.ResumeTimer();

		this.exitButton.OnClick = ()=>
		{
			this.ToggleMenu();
			this.levelSelector.UnloadLevel();
			Timer.singleton.StopTimer();
			UIController.singleton.GoToScreen<LevelselectionscreenManager>();
		};
		this.recenterButton.OnClick = this.isMenuActive ? ()=> OnRecenter?.Invoke() : empty;
		this.helpButton.OnClick = this.isMenuActive ? ()=> UIController.singleton.GoToScreen<HelpscreenManager>() : empty;

		this.pauseToggle.SetSprite(this.toggleSprites[this.isMenuActive ? 0 : 1]);

		System.Collections.IEnumerator Move()
		{
			float timer = 0.0f;
			float from = this.isMenuActive ? -this.menuTransform.sizeDelta.x : 0.0f;
			float to = this.isMenuActive ? 0.0f : -this.menuTransform.sizeDelta.x;
			float delta = to - from;

			Vector3 position = this.menuTransform.anchoredPosition;
			position.x = from;
			this.menuTransform.anchoredPosition = position;

			while(timer < 0.5f)
			{
				float step = delta * this.curve.Evaluate(timer / 0.5f);
				position.x = from + step;
				this.menuTransform.anchoredPosition = position;

				timer+=Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}

			position.x = to;
			this.menuTransform.anchoredPosition = position;
		}

		StartCoroutine(Move());
	}
	
	protected override void OnScreenDisabled()
	{
		base.OnScreenDisabled();
		if(!Application.isPlaying)
			return;

		this.backwardButton.OnButtonDown = null;
		this.backwardButton.OnButtonUp = null;

		this.forwardButton.OnButtonDown = null;
		this.forwardButton.OnButtonUp = null;

		this.leanForwardsButton.OnButtonDown = null;
		this.leanForwardsButton.OnButtonUp = null;

		this.leanBackwardsButton.OnButtonDown = null;
		this.leanBackwardsButton.OnButtonUp = null;
		this.recenterButton.OnClick = null;
	}
};