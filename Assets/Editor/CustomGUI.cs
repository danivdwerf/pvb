public static class CustomGUI
{
    public static bool ClickableLabel(string label, UnityEngine.Color colour = default)
    {
        UnityEngine.GUIStyle style = new UnityEngine.GUIStyle(UnityEngine.GUI.skin.button);
        style.normal.background = null;
        style.hover.background = null;
        style.active.background = null;
        style.fixedWidth = 0;

        style.normal.textColor = colour;
        style.alignment = UnityEngine.TextAnchor.MiddleLeft;

        return UnityEngine.GUILayout.Button(label, style);
    }
};