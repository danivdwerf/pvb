/// <summary>
/// Class to hold game data.
/// </summary>
public static class DATA
{
    /// <summary>
    /// Array of available levels
    /// </summary>
    public static LevelData[] LEVEL_DATA;
    /// <summary>
    /// ID of the current active level
    /// </summary>
    public static uint CURRENT_LEVEL;
    /// <summary>
    /// True if level is active and player has gone over the finishline
    /// </summary>
    public static bool LEVEL_COMPLETED;
};

/// <summary>
/// Class with the available tags.
/// </summary>
public static class TAGS
{
    public static string UNTAGGED = "Untagged";
    public static string RESPAWN = "Respawn";
    public static string FINISH = "Finish";
    public static string EDITOR_ONLY = "EditorOnly";
    public static string MAIN_CAMERA = "MainCamera";
    public static string PLAYER_TAG = "Player";
    public static string GAME_CONTROLLER = "GameController";
};