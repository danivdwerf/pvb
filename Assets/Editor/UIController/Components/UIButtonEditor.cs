using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIButton), true)]
[CanEditMultipleObjects]
public class UIButtonEditor : UISelectableEditor
{
	protected SerializedProperty pressedColour;
	protected SerializedProperty pressedSprite;
	protected SerializedProperty text;

	protected override void OnEnable()
	{
        base.OnEnable();
		
		this.pressedColour = this.serializedObject.FindProperty("pressedColour");
		this.pressedSprite = this.serializedObject.FindProperty("pressedSprite");
		this.text = this.serializedObject.FindProperty("text");
	}

	public override void OnInspectorGUI()
    {
        this.serializedObject.Update();

		EditorGUILayout.Space();
		this.CreateTransitions();

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(this.cursorTexture);
        EditorGUILayout.PropertyField(this.text);

        this.serializedObject.ApplyModifiedProperties();
    }

	protected override void CreateTransitions()
	{
		EditorGUILayout.LabelField("Transition", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(this.transitionType);
		if((this.transitionType.intValue & (int)UISelectable.TransitionType.COLOUR) != 0)
		{
			EditorGUILayout.PropertyField(this.normalColour);
			EditorGUILayout.PropertyField(this.pressedColour);
			EditorGUILayout.PropertyField(this.hoverColour);
			EditorGUILayout.PropertyField(this.disabledColour);
		}

		if((this.transitionType.intValue & (int)UISelectable.TransitionType.SPRITE) != 0)
		{
			EditorGUILayout.PropertyField(this.pressedSprite);
			EditorGUILayout.PropertyField(this.hoverSprite);
			EditorGUILayout.PropertyField(this.disabledSprite);
		}
	}
};