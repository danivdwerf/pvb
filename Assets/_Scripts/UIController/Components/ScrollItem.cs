﻿using UnityEngine;

/// <summary>
/// Scroll item to use in the LevelSelectionManager. Item's scale is based on the distance to center.
/// </summary>
public class ScrollItem : MonoBehaviour
{
    [SerializeField]private float scaleMultiplier = default;
    [SerializeField]private float minScale = default;
    [SerializeField]private float maxScale = default;
    [SerializeField]private RectTransform score = default;

    private void Start()=>
        this.score.localScale = new Vector3(1.0f, 0.0f, 1.0f);

    private void Update()
    {
        float scale = Mathf.Abs(this.transform.position.x) / scaleMultiplier;
        scale = 1 - scale;

        if(scale < this.minScale)
            scale = this.minScale;

        if(scale > this.maxScale)
            scale = this.maxScale;

        this.transform.localScale = new Vector2(scale, scale);
    }

    public void Activate(bool active)
    {
        System.Collections.IEnumerator Transition()
        {
            float from = active ? 0.0f : 1.0f;
            float to = active ? 1.0f : 0.0f;
            float value = 0.0f;
            while(value < 1.0f)
            {
                this.score.localScale = new Vector3(1.0f, Mathf.Lerp(from, to, value), 1.0f);
                value += 0.1f;
                yield return new WaitForEndOfFrame();
            }
            this.score.localScale = new Vector3(1.0f, to, 1.0f);  
        };

        this.StartCoroutine(Transition());
    }
};