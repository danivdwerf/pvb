﻿using UnityEngine;

/// <summary>
/// Holds info about the Audioclips for the AudioManager
/// </summary>
[System.Serializable]
public class AudioClip
{
	[SerializeField]private string name = "New_audioclip";
	/// <summary>
	/// Name of the AudioClip
	/// </summary>
	public string Name{get=> this.name; set=> this.name = value;}

	[SerializeField]private int tag = 0;
	/// <summary>
	/// The tag of the AudioClip
	/// </summary>
	public int Tag{get=> this.tag; set=> this.tag = value;}

	[SerializeField]private UnityEngine.AudioClip clip = null;
	/// <summary>
	/// Unity's audioclip which will actually be played.
	/// </summary>
	public UnityEngine.AudioClip Clip{get=> this.clip; set=> this.clip = value;}

	[Range(0, 1)][SerializeField]private float volume = 1;
	/// <summary>
	/// How loud the AudioClip will be played
	/// </summary>
	public float Volume{get=> this.volume; set=> this.volume = value;}

	[Range(-3, 3)][SerializeField]private float pitch = 1;	
	/// <summary>
	/// How high(or low) the AudioClip will be played
	/// </summary>
	public float Pitch{get=> this.pitch; set=> this.pitch = value;}

	[Range(-1, 1)][SerializeField]private float panning = 0;
	/// <summary>
	/// Should the audio come from the left or the right speaker
	/// </summary>
	public float Panning{get=>this.panning; set=> this.panning = value;}

	[SerializeField]private bool loop = false;
	/// <summary>
	/// Should the AudioClip replay after finishing
	/// </summary>
	public bool Loop{get=> this.loop; set=> this.loop = value;}

	[SerializeField]private bool playFromStart = false;
	/// <summary>
	/// Should the AudioClip start playgin right from the start 
	/// </summary>
	public bool PlayFromStart{get=> this.playFromStart; set=> this.playFromStart = value;}
	
	[Range(0, 1)][SerializeField]private float spatialBlend = 0;
	/// <summary>
	/// How much this AudioSource is affected by 3D spatialisation calculations.
	/// </summary>
	public float SpatialBlend{get=> this.spatialBlend; set=> this.spatialBlend = value;}
	
	[SerializeField]private float minDistance = 0;
	/// <summary>
	/// The minimum distance to calculate the loudness from
	/// </summary>
	public float MinDistance{get=> this.minDistance; set=> this.minDistance = value;}

	[SerializeField]private float maxDistance = 10;
	/// <summary>
	/// The maximum distance the audio should be heard from.
	/// </summary>
	public float MaxDistance{get=> this.maxDistance; set=> this.maxDistance = value;}
	
	[SerializeField]private Transform sourcePosition = null;
	/// <summary>
	/// The position the audio is playing from
	/// </summary>
	public Transform SourcePosition{get=> this.sourcePosition; set=> this.sourcePosition = value;}
	/// <summary>
	/// The duration of the AudioClip in seconds
	/// </summary>
	public float Length{get=> this.clip.length;}
	/// <summary>
	/// Is the clip playing right now?
	/// </summary>
	/// <value></value>
	public bool IsPlaying{get=> this.source.isPlaying;}

	private AudioSource source = null;

	/// <summary>
	/// Set the audiosource variable
	/// </summary>
	/// <param name="source">The source to use</param>
	public void SetSource(AudioSource source)
	{
		this.source = source;
		this.source.playOnAwake = false;
		this.source.clip = this.clip;
		this.source.volume = this.volume;
		this.source.pitch = this.pitch;
		this.source.panStereo = this.panning;
		this.source.loop = this.loop;
		this.source.spatialBlend = this.spatialBlend;

		if(this.SpatialBlend > 0)
		{
			if(this.sourcePosition == null) 
				this.sourcePosition = this.source.transform;
			this.source.minDistance = this.minDistance;
			this.source.maxDistance = this.maxDistance;
			this.source.rolloffMode = AudioRolloffMode.Linear;
			this.source.transform.position = sourcePosition.position;
		}

		if(this.PlayFromStart) 
			this.source.Play();
	}

	/// <summary>
	/// Play the audioclip
	/// </summary>
	public void Play()
	{
		if(!this.source.isPlaying) 
			this.source.Play();
	}

	/// <summary>
	/// Replay the clip from start. (Will also play when clip is not playing already)
	/// </summary>
	public void Replay()
	{
		this.source.time = 0.0f;
		this.source.Play();
	}

	/// <summary>
	/// Stop the audioclip
	/// </summary>
	public void Stop() => this.source.Stop();

	/// <summary>
	/// Set the volume to a scaled value. (Used with Volume-slide)
	/// </summary>
	/// <param name="multiplier"></param>
	public void SetVolumeMultiplier(float multiplier)=> this.source.volume = multiplier * this.Volume;
	
	public override string ToString()=> $"[Audioclip {this.name}] volume: {this.volume} 3D sound: {this.spatialBlend > 0}";
};