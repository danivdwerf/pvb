using UnityEngine;

/// <summary>
/// Responsible for all the player's movement.
/// </summary>
public class BikeMovement : MonoBehaviour
{
    [Header("Debug")]
    [SerializeField]private bool drawPhysicsCalculations = default;

    [Header("Wheels")]
    [SerializeField]private Wheel frontWheel = default;
    [SerializeField]private Wheel rearWheel = default;

    [Header("Physics")]
    [SerializeField]private Vector3 centerOfMass = default;
    [SerializeField]private float gravity = default;

    [Header("Movement")]
    [SerializeField]private float maxSpeed = default;
    [SerializeField]private float acceleration = default;
    [SerializeField]private float deceleration = default;

    [Header("Leaning")]
    [SerializeField]private float leanForce = 10.0f;
    [SerializeField]private float minLeaningVelocity = 0.005f;
    [SerializeField]private float maxLeanAngle = 80.0f;

    private bool shouldUpdate;
    /// <summary>
    /// Enable/Disable if the script should listen to player input
    /// </summary>
    public bool ShouldUpdate{get=> this.shouldUpdate; set=> this.shouldUpdate = value;}

    private float velocity;
    /// <summary>
    /// The velocity the bike is moving with
    /// </summary>
    /// <value></value>
    public float Velocity{get=> this.velocity;}

    private float speed;
    private int moveDir;
    private int leanDir;
    private Rigidbody rigid;

    private void Awake()
    {
        this.rigid = this.GetComponent<Rigidbody>();
    }

    private void Start()
    {
        this.rigid.centerOfMass = this.centerOfMass;
        
        HUDscreenManager.OnDirectionDown += this.OnDirectionDown;
        HUDscreenManager.OnDirectionUp += this.OnDirectionUp;

        HUDscreenManager.OnLeanDown += this.OnLeanDown;
        HUDscreenManager.OnLeanUp += this.OnLeanUp; 
    }

    /// <summary>
    /// Reset the player and movement.
    /// </summary>
    public void ResetMovement()
    {
        this.speed = default;
        this.shouldUpdate = true;
        this.rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
    }

    private void OnDirectionDown(bool forward)
    {
        if(!Timer.singleton.IsKeepingTime)
            Timer.singleton.StartTimer(); 
       
        this.moveDir = forward ? 1 : -1;
    }
      

    private void OnLeanDown(bool forward)=> 
        this.leanDir = forward ? 1 : -1;
    private void OnDirectionUp()=>
        this.moveDir = 0;
    private void OnLeanUp()=> 
        this.leanDir = 0;

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            this.OnLeanUp();

        if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
            this.OnDirectionUp();

        if(!this.shouldUpdate || DATA.LEVEL_COMPLETED)
            return;
        
        // Add keyboard support for debugging in editor
        #if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            this.OnDirectionDown(true);

        if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            this.OnDirectionDown(false);

        if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            this.OnLeanDown(false);

        if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            this.OnLeanDown(true);

        #endif
        
        this.CalculateVelocity();

        // Lock the stupid local z axis
        Vector3 pos = this.transform.localPosition;
        pos.x = 0.0f;
        this.transform.localPosition = pos;
    }

    private void CalculateVelocity()
    {
        if(this.moveDir.Equals(0))
            this.Deceleration();
        else
        {
            if(this.moveDir.Equals(1) && this.speed < this.maxSpeed)
                this.speed += this.acceleration * Time.deltaTime;
            
            if(this.moveDir.Equals(-1) && this.speed > -this.maxSpeed/2)
                this.speed -= this.acceleration/2 * Time.deltaTime;
        }
    }

    private void OnDrawGizmos()
    {
        if(!this.drawPhysicsCalculations)
            return;
        
        Gizmos.color = Color.blue;
        float angle = Mathf.DeltaAngle(0.0f, this.transform.localEulerAngles.z);

        Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 back = this.transform.right * Mathf.Sin(angle * Mathf.Deg2Rad);
        Vector3 dir = (up + back) * this.gravity;
        Gizmos.DrawLine(this.transform.position, this.transform.position + dir);

        if(this.rigid)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(this.rigid.position + this.rigid.centerOfMass, 0.01f);
        }
    }

    private void FixedUpdate()
    {
        if(!this.shouldUpdate)
            return;  

        if(DATA.LEVEL_COMPLETED)
        {
            if(this.speed > 0.01f)
                this.Deceleration();
            else
                return;
        }
        
        // Calculate the x rotation of the object
        float angle = Mathf.DeltaAngle(0.0f, this.transform.localEulerAngles.z);

        // Apply gravity based on rotation
        Vector3 dir = this.ApplyGravity(angle);

        // Update movement
        Vector3 movement = this.UpdateMovement();

        // Rotate wheels based on velocity
        this.velocity = movement.z + dir.z;
        this.frontWheel.RotateWithVelocity(this.velocity);
        this.rearWheel.RotateWithVelocity(this.velocity);
        

        // UnityEngine.Debug.Log($"{Mathf.Abs(this.velocity)} >= {this.minLeaningVelocity}");
        // Lean bicycle
        if(!this.leanDir.Equals(0) && Mathf.Abs(this.velocity) >= this.minLeaningVelocity)
            this.UpdateLeaning();

        if(!angle.IsBetween(-this.maxLeanAngle, this.maxLeanAngle))
        {
            this.shouldUpdate = false;
            this.rigid.constraints = RigidbodyConstraints.None;
            ResetPlayer.singleton.ResetMovementAndPosition();
            AudioManager.singleton.PlayClip($"bicycle_fall_{Random.Range(0, 2)}");
        }
    }

    private Vector3 ApplyGravity(float angle)
    {   
        if(!this.frontWheel.IsGrounded && !this.rearWheel.IsGrounded)
            return new Vector3();

        Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 back = this.transform.right * Mathf.Sin(angle * Mathf.Deg2Rad);
        Vector3 dir = (up + back) * this.gravity * Time.deltaTime;
        this.rigid.MovePosition(this.rigid.position + dir);
        
        return dir;
    }

    private Vector3 UpdateMovement()
    {
        Vector3 movement = -this.transform.right * speed * Time.deltaTime;
        if(this.rearWheel.IsGrounded)
            this.rigid.MovePosition(this.rigid.position + movement);

        return movement;
    }

    private void UpdateLeaning()
    {
        Transform wheel = (this.leanDir.Equals(1) ? this.rearWheel : this.frontWheel).transform;
        Vector3 force = (Vector3.up * this.leanForce) * Time.deltaTime;
        this.rigid.AddForceAtPosition(force, wheel.position, ForceMode.Impulse);
    }

    private void Deceleration()
    {
        float deceleration = this.deceleration * Time.deltaTime;

        if(this.speed > deceleration)
           this.speed -= deceleration;
        else if(this.speed < -deceleration)
            this.speed += deceleration;
        else
            this.speed = 0.0f;
    }
};