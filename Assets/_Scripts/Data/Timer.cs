﻿using UnityEngine;

/// <summary>
/// Class to keep track of the time the player takes to finish a level
/// </summary>
public class Timer : MonoBehaviour
{
	public static Timer singleton;
	
	#region Actions
	/// <summary>
	/// Called when the timer is started
	/// </summary>
	public static System.Action OnTimerStart;
	/// <summary>
	/// Called when the time is updated
	/// </summary>
	public static System.Action<float> OnTimerTick;
	/// <summary>
	/// Called when the timer is stopped
	/// </summary>
	public static System.Action<float> OnTimerEnd;
	#endregion

	private bool KeepTime;
	/// <summary>
	/// Check if timer is keeping time.
	/// </summary>
	/// <value></value>
	public bool IsKeepingTime{get=> this.KeepTime;}

	private float currentTime;
	/// <summary>
	/// Get the current time
	/// </summary>
	public float CurrentTime{get=> this.currentTime;}

	private void Awake()
	{
		if(singleton != null && singleton != this)
			Destroy(this);
		singleton = this;
	}

	/// <summary>
	/// Start tracking time.
	/// </summary>
	public void StartTimer(bool forceRestart = false)
	{
		if(this.KeepTime && !forceRestart)
			return;
		
		OnTimerStart?.Invoke();
		this.currentTime = default;
		this.KeepTime = true;
	}

	private void Update()
	{
		if(!this.KeepTime)
			return;
			
        this.currentTime += Time.deltaTime;
		OnTimerTick?.Invoke(this.currentTime);
	}

	/// <summary>
	/// Stop tracking time.
	/// </summary>
	public void StopTimer()
	{
		if(!this.KeepTime)
		{
			UnityEngine.Debug.LogError("StartTimer has not been called yet");
			return;
		}

		this.KeepTime = false;
		OnTimerEnd?.Invoke(this.currentTime);
		this.currentTime = 0.0f;
	}


	/// <summary>
	/// Pause the timer.
	/// </summary>
	public void PauseTimer()
	{
		if(!this.KeepTime)
			return;

		this.KeepTime = false;
	}

	/// <summary>
	/// Resume the timer.
	/// </summary>
	public void ResumeTimer()
	{
		if(this.currentTime.Equals(0.0f))
		{
			UnityEngine.Debug.Log("StartTimer has not been called yet");
			return;
		}
		this.KeepTime = true;
	}
};