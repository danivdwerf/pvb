﻿using UnityEngine;
using UnityEngine.UI;

using UnityEditor;

/// <summary>
/// Clickable button for Unity's canvas
/// </summary>
public class UIButton : UISelectable, UnityEngine.EventSystems.IPointerDownHandler, UnityEngine.EventSystems.IPointerUpHandler
{
	#if UNITY_EDITOR
	[MenuItem("GameObject/UI/UIController/UIButton", false, 20)]
	public static void CreateImageButton(MenuCommand command)
	{
		GameObject go = new GameObject("[BUTTON] Button");
		GameObjectUtility.SetParentAndAlign(go, command.context as GameObject);
        Undo.RegisterCreatedObjectUndo(go, $"Create {go.name}");
        Selection.activeObject = go;

		go.AddComponent(typeof(Image));
		go.AddComponent(typeof(UIButton));

		GameObject label = new GameObject("[TEXT] Label");
		label.transform.SetParent(go.transform, false);

		Text text = label.AddComponent(typeof(Text)) as Text;
		text.alignment = TextAnchor.MiddleCenter;
		text.rectTransform.anchorMax = new Vector2(1.0f, 1.0f);
		text.rectTransform.anchorMin = new Vector2(0.0f, 0.0f);
		text.rectTransform.sizeDelta = new Vector2(0.0f, 0.0f);
		text.text = "Button";
		text.color = new Color32(34, 34, 34, 255);
	}
	#endif

	#region Callbacks
	/// <summary>
	/// Callback when the button is clicked
	/// </summary>
	public System.Action OnClick;

	/// <summary>
	/// Callback when the button is pressed down
	/// </summary>
	public System.Action OnButtonDown;

	/// <summary>
	/// Callback when the button is let go
	/// </summary>
	public System.Action OnButtonUp;
	#endregion

	#region Transitions
	#region Colours
	[SerializeField]private Color pressedColour = Color.white;
	#endregion

	#region Sprites
	[SerializeField]private Sprite pressedSprite = null;
	#endregion
	#endregion

	[SerializeField]private Text text = null;
	/// <summary>
	/// Get/Set the button's label
	/// </summary>
	/// <value></value>
	public string Text
	{
		get => this.text is null ? string.Empty : this.text.text; 
		set
		{
			if(this.text != null)
				this.text.text = value;
		}
	}
	
	private Vector2 downPosition;

	private void Reset()=> 
		this.text = this.gameObject.GetComponentInChildren<Text>();
	private void OnValidate()=> 
		this.text = this.gameObject.GetComponentInChildren<Text>();

	protected override void Awake()
	{
		base.Awake();
		this.text = this.gameObject.GetComponentInChildren<Text>();
	}

	#region Click/Down/Up
	public void OnPointerDown(UnityEngine.EventSystems.PointerEventData e)
	{
		if(e.button != UnityEngine.EventSystems.PointerEventData.InputButton.Left || !this.enabled) 
			return;

		this.downPosition = e.position;
		if((this.transitionType & TransitionType.SPRITE) != 0)
			this.spriteInfo.SetValue(this.targetGraphic, this.pressedSprite);

		if((this.transitionType & TransitionType.COLOUR) != 0)
			this.targetGraphic.color = this.pressedColour;

		this.OnButtonDown?.Invoke();
	}

	public void OnPointerUp(UnityEngine.EventSystems.PointerEventData e)
	{
		if(e.button != UnityEngine.EventSystems.PointerEventData.InputButton.Left || !this.enabled) 
			return;
		
		if(this.downPosition.Equals(e.position))
			this.OnClick?.Invoke();
		
		this.downPosition = default;
		if((this.transitionType & TransitionType.SPRITE) != 0)
			this.spriteInfo.SetValue(this.targetGraphic, this.normalSprite);

		if((this.transitionType & TransitionType.COLOUR) != 0)
			this.targetGraphic.color = this.normalColour;

		this.OnButtonUp?.Invoke();
	}
	#endregion

	/// <summary>
	/// Method to tell the button it is pressed
	/// </summary>
	public override void Submit()=> 
		this.OnClick?.Invoke();
};